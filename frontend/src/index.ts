import { router } from './app';
import './assets/sass/style.scss';

window.addEventListener('load', router);
window.addEventListener('hashchange', router);
