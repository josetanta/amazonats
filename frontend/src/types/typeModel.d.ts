declare module 'type-models' {
	type Product = {
		_id: string | number;
		name: string;
		category: string;
		image: string;
		price: number;
		brand: string;
		rating: number;
		numReviews: number;
		countInStock: number;
		message?: string;
	};

	type Item = {
		product: string | number;
		name: string;
		image: string;
		price: number;
		countInStock: number;
		qty: number;

		category?: string;
		brand?: string;
		rating?: number;
		numReviews?: number;
	};

	type CartItem = {
		(item: Item, forceUpdate?: boolean): void | Item;
	};

	type UserType = {
		email: string;
		password: string;
		name?: string;
	};

	type ShippingType = {
		address: string;
		city: string;
		postalCode: string;
		country: string;
	};

	type PaymentType = { paymentMethod: string };

	type Order = {
		orderItems: Item[];
		shipping: ShippingType;
		payment: PaymentType;
		itemsPrice: number;
		shippingPrice: number;
		taxPrice: number;
		totalPrice: number;
	};

	type Paypal = {
		clientId: string;

		message?: string;
	};

	type UserResponseBackend = {
		_id: string;
		name: string;
		email: string;
		password: string;
		isAdmin: boolean;
		token: string;

		message?: string;
	};

	type OrderResponseBackend = {
		order?: {
			_id: string;
			orderItems: Item[];
			user: UserResponseBackend;
			shipping: ShippingType;
			payment: PaymentType;

			itemsPrice: number;
			taxPrice: number;
			shippingPrice: number;
			totalPrice: number;

			isPaid?: boolean;
			paidAt?: typeof Date;
			isDelivered?: boolean;
			deliveredAt?: typeof Date;
			createdAt?: typeof Date;
		};

		message?: string;
		error?: string;
	};

	type PaymentResultType = {
		orderID: string | number;
		payerID: string | number;
		paymentID: string | number;
	};

	type ErrorResponse = {
		message?: string;
		error?: string;
	};
}
