declare module 'type-screens' {
	type PropsComponentType = {
		step1?: boolean;
		step2?: boolean;
		step3?: boolean;
		step4?: boolean;
		value?: number | string;
		text?: string;
	};

	type ScreenType = {
		render(): string | Promise<string>;
		afteRender?(): string | void | Promise<string | void>;
		nameScreen?: string;
	};

	type ComponentTypeAsync = {
		// render(props?: PropsComponentType): string | Promise<string>;

		render(props?: any): string | Promise<string>;
		afterRender?(): string | void | Promise<string | void>;
	};

	type ComponentType = ComponentTypeAsync;
}
