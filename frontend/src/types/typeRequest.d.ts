declare module 'type-requests' {
	import { ScreenType } from 'type-screens';

	export type RequestURL = {
		id: number | string;
		verb: string;
		resource: string;
	};

	export type Routes = {
		[url: string]: ScreenType;
	};
}
