import { ScreenType } from 'type-screens';
import { RequestURL } from 'type-requests';

import { getCartItems } from './localStorage';

export function parseRequestURL(): RequestURL {
	const url = document.location.hash.toLowerCase();
	const request = url.split('/');

	return {
		resource: request[1],
		id: request[2],
		verb: request[3],
	};
}

export async function reRender(componentScreen: ScreenType): Promise<void> {
	const mainContainerDOM = <Element>document.getElementById('main-container');
	mainContainerDOM.innerHTML = await componentScreen.render();

	if (componentScreen.afteRender) await componentScreen.afteRender();
}

export function showLoading(): void {
	document.getElementById('loading-overlay')?.classList.add('active');
}
export function hideLoading(): void {
	document.getElementById('loading-overlay')?.classList.remove('active');
}

export function showMessage(msg: string, callback?: () => void): void {
	(<HTMLDivElement>document.getElementById('message-overlay')).innerHTML = `
    <div>
      <div id="message-overlay-content">${msg}</div>
      <button id="message-overlay-close-button">OK</button>
    </div>
  `;
	document.getElementById('message-overlay')?.classList.add('active');
	document
		.getElementById('message-overlay-close-button')
		?.addEventListener('click', () => {
			document.getElementById('message-overlay')?.classList.remove('active');

			callback?.();
		});
}

export function redirectUser(): void {
	if (getCartItems().length !== 0) document.location.hash = '/shipping';
	else document.location.hash = '/';
}
