import {
	Item,
	PaymentType,
	ShippingType,
	UserResponseBackend,
} from 'type-models';

export const getCartItems = (): Item[] =>
	localStorage.getItem('cartItems')
		? JSON.parse(String(localStorage.getItem('cartItems')))
		: [];

export function setCartItems(cartItems: Item[]): void {
	localStorage.setItem('cartItems', JSON.stringify(cartItems));
}

export function setUserInfo(user: UserResponseBackend): void {
	localStorage.setItem('userInfo', JSON.stringify(user));
	sessionStorage.setItem('userInfo', JSON.stringify(user));
}

export function getUserInfo(): UserResponseBackend {
	return localStorage.getItem('userInfo')
		? JSON.parse(<string>localStorage.getItem('userInfo'))
		: null;
}

export function clearUser(): void {
	localStorage.removeItem('userInfo');
}

export function getShipping(): ShippingType {
	return localStorage.getItem('shipping')
		? JSON.parse(String(localStorage.getItem('shipping')))
		: {
				address: '',
				city: '',
				postalCode: '',
				country: '',
		  };
}

export function setShipping(shipping: ShippingType): void {
	localStorage.setItem('shipping', JSON.stringify(shipping));
}

export function getPayment(): PaymentType {
	return localStorage.getItem('payment')
		? JSON.parse(String(localStorage.getItem('payment')))
		: {
				paymentMethod: 'paypal',
		  };
}

export function setPayment(payment: PaymentType): void {
	localStorage.setItem('payment', JSON.stringify(payment));
}

export function cleanCart(): void {
	localStorage.removeItem('cartItems');
}
