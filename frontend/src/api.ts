import axios, { AxiosResponse } from 'axios';
import * as types from 'type-models';
import { API_URL } from './config';
import { getUserInfo } from './localStorage';

export const getProduct = async (productId: number): Promise<types.Product> => {
	const response = await axios.get<types.Product>(
		`${API_URL}/api/v1/products/${productId}`
	);

	if (response.statusText !== 'OK') {
		throw new Error(response.data.message);
	}

	return response.data;
};

export async function signin(user: types.UserType): Promise<any> {
	try {
		const response = await axios.post<types.UserResponseBackend>(
			`${API_URL}/api/v1/users/signin`,
			user,
			{
				headers: {
					'Content-Type': 'application/json',
				},
			}
		);
		if (response.statusText !== 'OK') {
			throw new Error(response.data.message);
		}
		return response.data;
	} catch (error: any) {
		return { error: error.response.data.message || error.message };
	}
}

export async function register(user: types.UserType): Promise<any> {
	try {
		const response = await axios.post<types.UserResponseBackend>(
			`${API_URL}/api/v1/users/register`,
			user,
			{
				headers: {
					'Content-Type': 'application/json',
				},
			}
		);
		if (response.statusText !== 'OK') {
			throw new Error(response.data.message);
		}
		return response.data;
	} catch (error: any) {
		return { error: error.response.data.message || error.message };
	}
}

export async function update(user: types.UserType): Promise<any> {
	try {
		const { _id, token } = getUserInfo();

		const response = await axios.put<types.UserResponseBackend>(
			`${API_URL}/api/v1/users/${_id}`,
			user,
			{
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`,
				},
			}
		);
		if (response.statusText !== 'OK') {
			throw new Error(response.data.message);
		}
		return response.data;
	} catch (error: any) {
		return { error: error.response.data.message || error.message };
	}
}

export async function createOrder(
	order: types.Order
): Promise<types.OrderResponseBackend> {
	try {
		const { token } = getUserInfo();

		const response = await axios.post<types.OrderResponseBackend>(
			`${API_URL}/api/v1/orders/`,
			order,
			{
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`,
				},
			}
		);

		if (response.status !== 201) throw new Error(response.data.message);

		return response.data;
	} catch (error: any) {
		return {
			error: error.response ? error.response.data.message : error.message,
		};
	}
}

export async function getOrder(
	id: number | string
): Promise<types.OrderResponseBackend> {
	try {
		const { token } = getUserInfo();

		const response = await axios.get<types.OrderResponseBackend>(
			`${API_URL}/api/v1/orders/${id}`,
			{
				headers: {
					Authorization: `Bearer ${token}`,
					'Content-Type': 'application/json',
				},
			}
		);
		if (response.status !== 200) throw new Error(response.data.message);

		return response.data;
	} catch (err: any) {
		return { error: err.message };
	}
}

export async function getPaypalClientId(): Promise<types.Paypal> {
	const response = await axios.get<types.Paypal>(
		`${API_URL}/api/v1/paypal/client_id/`
	);

	return response.data;
}

export async function payOrder(
	orderId: string | number,
	paymentResult: types.PaymentResultType
): Promise<types.OrderResponseBackend> {
	try {
		const { token } = getUserInfo();
		const response = await axios.put<types.OrderResponseBackend>(
			`${API_URL}/api/v1/orders/${orderId}/pay`,
			paymentResult,
			{
				headers: {
					Authorization: `Bearer ${token}`,
					'Content-Type': 'application/json',
				},
			}
		);

		if (response.status !== 200) throw new Error(response.data.message);

		return response.data;
	} catch (error: any) {
		return {
			error: error.response ? error.response.data.message : error.message,
		};
	}
}

export async function getMyOrders(): Promise<
	AxiosResponse<types.OrderResponseBackend['order'][] & { error: string }>
> {
	try {
		const userInfo = getUserInfo();
		const response = await axios.get<
			types.OrderResponseBackend['order'][] & { error: string } & {
				message: string;
			}
		>(`${API_URL}/api/v1/orders/mine`, {
			headers: {
				Authorization: `Bearer ${userInfo.token}`,
				'Content-Type': 'application/json',
			},
		});
		if (response.status != 200) throw new Error(response.data.message);
		return response;
	} catch (error: any) {
		return error.response;
	}
}

export async function getProducts(): Promise<AxiosResponse<types.Product[]>> {
	const res = await axios.get<types.Product[]>(
		'http://localhost:5057/api/v1/products/'
	);

	return res;
}
