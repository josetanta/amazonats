import { Product } from 'type-models';
import { ComponentType } from 'type-screens';
import Rating from './Rating';

const ProductComponent: ComponentType = {
	render: (props: { product: Product }) => {
		return `
			 <div class="product">
				<a href="/#/product/${props.product._id}">
					<img src="${props.product.image}" alt="${props.product.name}" />
				</a>
				<div class="product-name">
					<a href="/#/product/${props.product._id}">${props.product.name}</a>
				</div>
				<div class="product-rating">
					${Rating.render({
						value: props.product.rating,
						text: `${props.product.numReviews} reviews`,
					})}
				</div>
				<div class="product-brand">${props.product.brand}</div>
				<div class="product-price">$${props.product.price}</div>
			</div>
		`;
	},
};

export default ProductComponent;
