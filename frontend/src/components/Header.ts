import { ComponentType } from 'type-screens';

import { getUserInfo } from '../localStorage';

const Header: ComponentType = {
	render: () => {
		const user = getUserInfo();

		return `
			<div class="brand">
				<a href="/#/">AmazonaTS</a>
			</div>
			<div>
			${
				user
					? `<a href="/#/profile">${user.name}</a>`
					: `<a href="/#/signin">Sign-in</a>`
			}
				<a href="/#/cart">Cart</a>
			</div>
		`;
	},
};

export default Header;
