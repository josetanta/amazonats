import { ScreenType } from 'type-screens';
import { CartItem, Item } from 'type-models';

import { getProduct } from '../api';
import { getCartItems, setCartItems } from '../localStorage';
import { parseRequestURL, reRender } from '../utils';

const addToCart: CartItem = (item, forceUpdate = false): void => {
	let cartItems = getCartItems();
	const existItem = cartItems.find((x) => x.product === item.product);
	if (existItem) {
		if (forceUpdate) {
			cartItems = cartItems.map((x) =>
				x.product === existItem.product ? item : x
			);
		}
	} else {
		cartItems = [...cartItems, item];
	}
	setCartItems(cartItems);

	if (forceUpdate) {
		reRender(CartScreen);
	}
};

const removeFromCart = (id: number | string) => {
	setCartItems(getCartItems().filter((x) => x.product !== id));

	if (id === parseRequestURL().id) document.location.hash = '/cart';
	else reRender(CartScreen);
};

const CartScreen: ScreenType = {
	afteRender: () => {
		const qtySelectsDOM = <HTMLCollectionOf<HTMLSelectElement>>(
			document.getElementsByClassName('qty-select')
		);
		Array.from(qtySelectsDOM).forEach((qtySelect) => {
			qtySelect.addEventListener('change', (e) => {
				const item = <Item>(
					getCartItems().find((x) => x.product === qtySelect.id)
				);
				addToCart(
					{ ...item, qty: Number((<HTMLOptionElement>e.target).value) },
					true
				);
			});
		});

		const deleteButtonsDOM = document.getElementsByClassName(
			'delete-button'
		) as HTMLCollectionOf<HTMLButtonElement>;

		Array.from(deleteButtonsDOM).forEach((deleteButton) => {
			deleteButton.addEventListener('click', () => {
				removeFromCart(deleteButton.id);
			});
		});
		document
			.getElementById('checkout-button')
			?.addEventListener('click', () => {
				document.location.hash = '/signin';
			});
	},
	render: async () => {
		document.title = 'Cart Products';

		const request = parseRequestURL();
		if (request.id) {
			const product = await getProduct(Number(request.id));
			addToCart({
				product: product._id,
				name: product.name,
				image: product.image,
				price: product.price,
				countInStock: product.countInStock,
				qty: 1,
			});
		}

		const cartItems = getCartItems();

		return `
      <div class="content cart">
        <div class="cart-list">
          <ul class="cart-list-container">
            <li>
              <h3>Shipping Cart</h3>
              <div>Price</div>
            </li>
            ${
							cartItems.length === 0
								? `<div>Cart is Empty. <a href="/#/">Go to shipping</a></div>`
								: cartItems
										.map(
											(item) => `
                  <li>
                    <div class="cart-image">
                      <img src="${item.image}" alt="${item.name}"/>
                    </div>
                    <div class="cart-name">
                      <div>
                        <a href="${`/#/product/${item.product}`}">
                          ${item.name}
                        </a>
                      </div>
                    </div>
                    <div>
                      Qty:
                      <select class="qty-select" id="${item.product}">
                        ${[...Array(item.countInStock).keys()].map((x) =>
													item.qty === x + 1
														? `<option selected value="${x + 1}">
															${x + 1}</option>`
														: `<option value="${x + 1}">${x + 1}</option>`
												)}
                      </select>
                      <button type="button" class="delete-button" id="${
												item.product
											}">
                        Delete
                      </button>
                    </div>
                    <div class="cart-price">
                      $${item.price}
                    </div>
                  </li>
                `
										)
										.join('\n')
						}
          </ul>
        </div>
        <div class="cart-action">
          <h3>
            Subtotal (${cartItems.reduce((a, c) => a + c.qty, 0)} items)
            :
            $${cartItems.reduce((a, c) => a + c.qty * c.price, 0).toFixed(2)}
          </h3>
          <button class="primary fw" id="checkout-button">
            Proceed to checkout
          </button>
        </div>
      </div>
		`;
	},
};

export default CartScreen;
