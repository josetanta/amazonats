import { ScreenType } from 'type-screens';

import { hideLoading, parseRequestURL, showLoading } from '../utils';
import { getProduct } from '../api';
import Rating from '../components/Rating';

const ProductScreen: ScreenType = {
	afteRender: () => {
		const request = parseRequestURL();

		document.getElementById('add-button')?.addEventListener('click', () => {
			document.location.hash = `/cart/${request.id}`;
		});
	},

	render: async () => {
		const request = parseRequestURL();

		showLoading();
		const product = await getProduct(Number(request.id));

		if (product.message) {
			return `
        <h4>Product not Found</h4>
      `;
		}
		document.title = product.name;
		hideLoading();

		return `
			<div class="content">
        <div class="back-to-result">
          <a href="/#/">Back to result</a>
        </div>
        <div class="details">
          <div class="details-image">
            <img src="${product.image}" alt="${product.name}"/>
          </div>
          <div class="details-info">
            <ul>
              <li>
                <h1>${product.name}</h1>
              </li>
              <li>
                ${Rating.render({
									value: product.rating,
									text: `${product.numReviews} review(s)`,
								})}
              </li>
              <li>
                Price: <strong>$${product.price}</strong>
              </li>
              <li>
                Description
                <div>
                  ${product.name}
                </div>
              </li>
            </ul>
          </div>
          <div class="details-action">
            <ul>
              <li>
                Price: $${product.price}
              </li>
              <li>
                ${
									product.countInStock > 0
										? `<span class="success">In Stock</span>`
										: `<span class="error">Unavailable</span>`
								}
              </li>
              <li>
                <button id="add-button" class="primary fw">Add to cart</button>
              </li>
            </ul>
          </div>
        </div>
      </div>
		`;
	},
};

export default ProductScreen;
