import CheckoutSteps from '../components/CheckoutSteps';
import { ScreenType } from 'type-screens';
import * as localSrg from '../localStorage';
import * as utils from '../utils';

const PaymentScreen: ScreenType = {
	afteRender: () => {
		document
			.getElementById('payment-form')
			?.addEventListener('submit', async (e) => {
				e.preventDefault();

				const PaymentData = {
					paymentMethod: (<HTMLInputElement>(
						document.querySelector('input[name="payment-method"]:checked')
					)).value,
				};

				localSrg.setPayment(PaymentData);

				document.location.hash = '/placeorder';
			});
	},
	render: () => {
		const { name } = localSrg.getUserInfo();

		if (!name) utils.redirectUser();

		return /* html */ `
    ${CheckoutSteps.render({ step1: true, step2: true, step3: true })}
			<div class="form-container">
				<form id="payment-form">
					<ul class="form-items">
						<li>
							<h1>Payment</h1>
						</li>
						<li>
							<div>
								<input type="radio" name="payment-method" id="paypal" value="paypal" ckecked/>
								<label for="paypal">Paypal</label>
							</div>
						</li>
						<li>
							<div>
								<input type="radio" name="payment-method" id="stripe" value="stripe" />
								<label for="stripe">Stripe</label>
							</div>
						</li>
						<li>
							<button type="submit" class="primary">Continue</button>
						</li>
					</ul>
				</form>
			</div>
		`;
	},
};

export default PaymentScreen;
