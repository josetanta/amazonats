import { ScreenType } from 'type-screens';

import { register } from '../api';
import { getUserInfo, setUserInfo } from '../localStorage';
import * as utils from '../utils';

const RegisterScreen: ScreenType = {
	afteRender: () => {
		document
			.getElementById('register-form')
			?.addEventListener('submit', async (e) => {
				e.preventDefault();

				utils.showLoading();
				const data = await register({
					email: (<HTMLInputElement>document.getElementById('email')).value,
					name: (<HTMLInputElement>document.getElementById('name')).value,
					password: (<HTMLInputElement>document.getElementById('password'))
						.value,
				});
				utils.hideLoading();

				if (data.error) {
					utils.showMessage(data.error);
				} else {
					setUserInfo(data);
					utils.redirectUser();
				}
			});
	},
	render: () => {
		if (getUserInfo()) utils.redirectUser();

		return `
			<div class="form-container">
				<form id="register-form">
					<ul class="form-items">
						<li>
							<h1>Create  Account</h1>
						</li>
						<li>
							<label for="email">Email</label>
							<input type="email" name="email" id="email"/>
						</li>
						<li>
							<label for="name">Name</label>
							<input type="name" name="name" id="name"/>
						</li>
						<li>
							<label for="password">Password</label>
							<input type="password" name="password" id="password" autocomplete="off"/>
						</li>
						<li>
							<label for="re-password">Repeat Password</label>
							<input type="password" name="re-password" id="re-password" autocomplete="off"/>
						</li>
						<li>
							<button type="submit" class="primary">Register</button>
						</li>
						<li>
							<div>
								Already hace a account?
								<a href="/#/signin">Sign-In</a>
							</div>
						</li>
					</ul>
				</form>
			</div>
		`;
	},
};

export default RegisterScreen;
