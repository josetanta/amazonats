import { ScreenType } from 'type-screens';

import CheckoutSteps from '../components/CheckoutSteps';
import * as localSrg from '../localStorage';
import * as utils from '../utils';

const ShippingScreen: ScreenType = {
	afteRender: () => {
		document
			.getElementById('shipping-form')
			?.addEventListener('submit', async (e) => {
				e.preventDefault();

				const shippingData = {
					address: (<HTMLInputElement>document.getElementById('address')).value,
					country: (<HTMLInputElement>document.getElementById('country')).value,
					city: (<HTMLInputElement>document.getElementById('city')).value,
					postalCode: (<HTMLInputElement>document.getElementById('postalCode'))
						.value,
				};

				localSrg.setShipping(shippingData);

				document.location.hash = '/payment';
			});
	},
	render: () => {
		const { name } = localSrg.getUserInfo();

		if (!name) utils.redirectUser();

		const { address, city, postalCode, country } = localSrg.getShipping();

		return `
    ${CheckoutSteps.render({ step1: true, step2: true })}
			<div class="form-container">
				<form id="shipping-form">
					<ul class="form-items">
						<li>
							<h1>Shipping</h1>
						</li>
						<li>
							<label for="address">Address</label>
							<input type="text" name="address" id="address" value="${address}"/>
						</li>
            <li>
							<label for="city">City</label>
							<input type="text" name="city" id="city" value="${city}"/>
						</li>
            <li>
							<label for="postalCode">Postal Code</label>
							<input type="text" name="postalCode" id="postalCode" value="${postalCode}"/>
						</li>
            <li>
							<label for="country">Country</label>
							<input type="text" name="country" id="country" value="${country}"/>
						</li>
						<li>
							<button type="submit" class="primary">Continue</button>
						</li>
					</ul>
				</form>
			</div>
		`;
	},
};

export default ShippingScreen;
