import { ScreenType } from 'type-screens';

import { hideLoading, showLoading } from '../utils';
import ProductComponent from '../components/Product';
import { getProducts } from '../api';

const HomeScreen: ScreenType = {
	nameScreen: 'Inicio',
	render: async () => {
		document.title = 'Inicio';

		showLoading();

		const response = await getProducts();

		hideLoading();

		if (!response || response.statusText !== 'OK') {
			return `<div>Error in getting </div>`;
		}

		const products = response.data;
		return `
      <ul class="products">
        ${products
					.map(
						(product) =>
							`
          <li>
            ${ProductComponent.render({ product })}
          </li>
        `
					)
					.join('\n')}
      </ul>
		`;
	},
};

export default HomeScreen;
