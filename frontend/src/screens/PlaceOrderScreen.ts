import { ScreenType } from 'type-screens';
import { OrderType } from 'type-models';

import CheckoutSteps from '../components/CheckoutSteps';
import { createOrder } from '../api';
import * as localStrg from '../localStorage';
import * as utils from '../utils';

function getConvertCartToOrder(): OrderType {
	const orderItems = localStrg.getCartItems();
	if (orderItems.length === 0) document.location.hash = '/cart';

	const shipping = localStrg.getShipping();
	if (!shipping.address) document.location.hash = '/shipping';

	const payment = localStrg.getPayment();
	if (!payment.paymentMethod) document.location.hash = '/payment';

	const itemsPrice = orderItems.reduce((a, c) => a + c.price * c.qty, 0);
	const shippingPrice = itemsPrice > 100 ? 0 : 10;
	const taxPrice = Math.round(0.15 * itemsPrice * 100) / 100;
	const totalPrice = Number((itemsPrice + shippingPrice + taxPrice).toFixed(2));

	return {
		orderItems,
		shipping,
		payment,
		itemsPrice,
		shippingPrice,
		taxPrice,
		totalPrice,
	};
}

const PlaceOrderScreen: ScreenType = {
	afteRender: async () => {
		document
			.getElementById('placeorder-button')
			?.addEventListener('click', async () => {
				const order = getConvertCartToOrder();

				utils.showLoading();
				const data = await createOrder(order);
				utils.hideLoading();

				if (data.error) utils.showMessage(data.error);

				localStrg.cleanCart();
				if (data.order) document.location.hash = '/order/' + data.order._id;
			});
	},

	render: () => {
		const {
			orderItems,
			shipping,
			payment,
			itemsPrice,
			shippingPrice,
			taxPrice,
			totalPrice,
		} = getConvertCartToOrder();

		return `
			<div>
				${CheckoutSteps.render({ step1: true, step2: true, step3: true, step4: true })}
				<div class="order">
					<div class="order-info">
						<div>
							<h2>Shipping</h2>
							<div>
								${shipping.address}, ${shipping.city}, ${shipping.postalCode}, ${
			shipping.country
		}
							</div>
						</div>
						<div>
							<h2>Payment</h2>
							<div>
								Payment Method: ${payment.paymentMethod}
							</div>
						</div>
						<div>
							<ul class="cart-list-container">
								<li>
									<h2>Shopping Cart</h2>
									<div>Price</div>
								</li>
								${orderItems.map(
									(item) => `
										<li>
											<div class="cart-image">
												<img src="${item.image}" alt="${item.name}"/>
											</div>
											<div class="cart-name">
												<div>
													<a href="/#/product/${item.product}" class="product-title">${item.name}</a>
												</div>
												<div>
													Qty: ${item.qty}
												</div>
											</div>
											<div class="cart-price">
												$${item.price}
											</div>
										</li>
									`
								)}
							</ul>
						</div>
					</div>
					<div class="order-action">
						<ul>
							<li><h2>Order Summary</h2></li>
							<li><div>Items</div><div>$ ${itemsPrice.toFixed(2)}</div></li>
							<li><div>Shipping</div><div>$ ${shippingPrice}</div></li>
							<li><div>Tax</div><div>$ ${taxPrice}</div></li>
							<li class="total"><div>Total Price</div><div>$ ${totalPrice}</div></li>
							<li>
								<button id="placeorder-button" class="primary fw">Place Order</button>
							</li>
						</ul>
					</div>
				</div>
			</div>
		`;
	},
};

export default PlaceOrderScreen;
