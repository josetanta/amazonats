import { ScreenType } from 'type-screens';

const Error404Screen: ScreenType = {
	render: () => {
		return `
			<h1>Not Found</h1>
		`;
	},
};

export default Error404Screen;
