import { ScreenType } from 'type-screens';
import { signin } from '../api';
import { getUserInfo, setUserInfo } from '../localStorage';
import * as utils from '../utils';

const SigninScreen: ScreenType = {
	afteRender: () => {
		document
			.getElementById('signin-form')
			?.addEventListener('submit', async (e) => {
				e.preventDefault();

				utils.showLoading();
				const data = await signin({
					email: (<HTMLInputElement>document.getElementById('email')).value,
					password: (<HTMLInputElement>document.getElementById('password'))
						.value,
				});
				utils.hideLoading();

				if (data.error) {
					utils.showMessage(data.error);
				} else {
					setUserInfo(data);
					utils.redirectUser();
				}
			});
	},
	render: () => {
		if (getUserInfo()) utils.redirectUser();
		return `
			<div class="form-container">
				<form id="signin-form">
					<ul class="form-items">
						<li>
							<h1>Sign-In</h1>
						</li>
						<li>
							<label for="email">Email</label>
							<input type="email" name="email" id="email"/>
						</li>
						<li>
							<label for="password">Password</label>
							<input type="password" name="password" id="password" autocomplete="off"/>
						</li>
						<li>
							<button type="submit" class="primary">Signin</button>
						</li>
						<li>
							<div>
								New User?
								<a href="/#/register">Create your account</a>
							</div>
						</li>
					</ul>
				</form>
			</div>
		`;
	},
};

export default SigninScreen;
