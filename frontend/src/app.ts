import { Routes } from 'type-requests';
import { ScreenType } from 'type-screens';

import Header from './components/Header';
import CartScreen from './screens/CartScreen';
import Error404Screen from './screens/Error404Screen';
import HomeScreen from './screens/HomeScreen';
import OrderScreen from './screens/OrderScreen';
import PaymentScreen from './screens/PaymentScreen';
import PlaceOrderScreen from './screens/PlaceOrderScreen';
import ProductScreen from './screens/ProductScreen';
import ProfileScreen from './screens/ProfileScreen';
import RegisterScreen from './screens/RegisterScreen';
import ShippingScreen from './screens/ShippingScreen';
import SigninScreen from './screens/SigninScreen';

import * as utils from './utils';

const mainDOM = document.getElementById('main-container') as HTMLElement;
const headerDOM = document.getElementById('header-container') as HTMLElement;

const routes: Routes = {
	'/': HomeScreen,
	'/product/:id': ProductScreen,
	'/cart/:id': CartScreen,
	'/order/:id': OrderScreen,
	'/signin': SigninScreen,
	'/register': RegisterScreen,
	'/profile': ProfileScreen,
	'/shipping': ShippingScreen,
	'/payment': PaymentScreen,
	'/placeorder': PlaceOrderScreen,
};

async function router(): Promise<void> {
	utils.showLoading();

	const request = utils.parseRequestURL();

	const parseURL =
		(request.resource ? `/${request.resource}` : '/') +
		(request.id ? '/:id' : '') +
		(request.verb ? `${request.verb}` : '');

	const screen: ScreenType = routes[parseURL] || Error404Screen;

	// Document Title
	if (screen.nameScreen) document.title = screen.nameScreen;

	headerDOM.innerHTML = await Header.render();
	Header.afterRender && (await Header.afterRender());

	mainDOM.innerHTML = await screen.render();
	screen.afteRender && (await screen.afteRender());

	utils.hideLoading();
}

export { router };
