module.exports = {
  module: {
    rules: [
      {
        test: /\.html$/,
        use: ['html-loader'],
      },
      {
        type: 'asset',
        test: /\.(png|jpe?g|svg|ico|gif)/,
      },
    ],
  },

  resolve: {
    extensions: ['.ts', '.json', '.js'],
  },
};
