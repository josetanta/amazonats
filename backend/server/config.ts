import dotenv from 'dotenv';

dotenv.config();

const MONGODB_URL = <string>process.env.MONGODB_URL;
const JWT_SECRET = <string>process.env.JWT_SECRET;
const PAYPAL_CLIENT_ID = <string>process.env.PAYPAL_CLIENT_ID;

export { MONGODB_URL, JWT_SECRET, PAYPAL_CLIENT_ID };
