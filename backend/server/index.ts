import express, { Application, NextFunction, Request, Response } from 'express';
import cors from 'cors';

import MongoDBConnection from '../db/conections';

import productRouter from '../routers/productRouter';
import userRouter from '../routers/userRouter';
import orderRouter from '../routers/orderRouter';
import paypalRouter from '../routers/paypalRouter';

class Server {
	private app: Application;
	private mongodb = new MongoDBConnection();
	private readonly port = '5057';
	private readonly apiPaths = {
		products: '/api/v1/products',
		users: '/api/v1/users',
		orders: '/api/v1/orders',
		paypal: '/api/v1/paypal',
	};
	constructor() {
		this.app = express();

		this.connectionsDB();
		this.middleware();
		this.routes();
	}

	connectionsDB(): void {
		this.mongodb
			.connectMongoDB()
			.then(() => {
				console.log('Connect MongoDB 🏝🎉');
			})
			.catch((err) => {
				console.log(err);
			});
	}

	middleware(): void {
		this.app.use(
			cors({
				credentials: true,
			})
		);

		this.app.use(middlewareError);

		this.app.use(express.json());

		this.app.use(express.static('public'));
	}

	routes(): void {
		this.app.use(this.apiPaths.products, productRouter);
		this.app.use(this.apiPaths.users, userRouter);
		this.app.use(this.apiPaths.orders, orderRouter);
		this.app.use(this.apiPaths.paypal, paypalRouter);
	}

	listen(): void {
		this.app.listen(this.port, (): void => {
			console.log(
				`Server is current on port http://localhost:${this.port} 🥑🥑🥑🥑😎`
			);
		});
	}
}

function middlewareError(
	err: any,
	req: Request,
	res: Response,
	_next: NextFunction
) {
	const status = err.name && err.name === 'ValidationError' ? 400 : 500;
	res.status(status).send({ message: err.message });
}

export default Server;
