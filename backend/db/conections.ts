import mongoose, { Mongoose } from 'mongoose';
import * as config from '../server/config';

class MongoDBConnection {
	private mongo: Mongoose;
	private MONGODB_URL = config.MONGODB_URL;

	constructor() {
		this.mongo = mongoose;
	}

	async connectMongoDB(): Promise<void> {
		await this.mongo.connect(this.MONGODB_URL, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
			useCreateIndex: true,
		});
	}
}

export default MongoDBConnection;
