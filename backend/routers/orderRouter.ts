import { Router } from 'express';

import * as controllers from '../controllers/orderController';
import { isAuth } from '../utils/utils';

const orderRouter = Router();

orderRouter
	.post('/', isAuth, controllers.createOrder)
	.get('/mine', isAuth, controllers.getOrdersByUserId)
	.get('/:id', controllers.getOrder)
	.put('/:id/pay', isAuth, controllers.payOrder);

export default orderRouter;
