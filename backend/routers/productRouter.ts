import { Router } from 'express';
import * as controller from '../controllers/productController';

const productRouter = Router();

productRouter.get('/', controller.getProducts);
productRouter.get('/:id', controller.getProduct);

export default productRouter;
