import { Router } from 'express';
import * as controller from '../controllers/userController';
import { isAuth } from '../utils/utils';

const userRouter = Router();

userRouter.get('/', controller.getUsers);
userRouter.put('/:id', isAuth, controller.update);
userRouter.post('/signin', controller.signin);
userRouter.post('/register', controller.register);
userRouter.get('/createadmin', controller.getCreateAdmin);

export default userRouter;
