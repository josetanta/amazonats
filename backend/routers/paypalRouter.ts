import { Router } from 'express';
import { getPaypalClientId } from '../controllers/paypalController';

const paypalRouter = Router();

paypalRouter.get('/client_id', getPaypalClientId);

export default paypalRouter;
