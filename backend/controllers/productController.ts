import { Request, Response } from 'express';
import products from '../utils/data';

export const getProducts = (req: Request, res: Response): void => {
  res.status(200).json(products);
};

export const getProduct = (req: Request, res: Response): void => {
  const { id } = req.params;

  const product = products.find((x) => x._id === id);

  if (product) res.status(200).json(product);
  else res.status(404).json({ message: 'Product not found' });
};
