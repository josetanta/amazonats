import { Request, Response } from 'express';
import asyncExpressHandler from 'express-async-handler';
import OrderModel from '../models/orderModel';

export const createOrder = asyncExpressHandler(
	async (req: Request, res: Response) => {
		const order = new OrderModel({
			orderItems: req.body.orderItems,
			user: req.body.user._id,
			shipping: req.body.shipping,
			payment: req.body.payment,
			itemsPrice: req.body.itemsPrice,
			taxPrice: req.body.taxPrice,
			totalPrice: req.body.totalPrice.toFixed(2),
			shippingPrice: req.body.shippingPrice,
		});
		const orderCreated = await order.save();

		res.status(201).json({ message: 'New Order Created', order: orderCreated });
	}
);

export const getOrder = asyncExpressHandler(
	async (req: Request, res: Response) => {
		const { id } = req.params;

		const order = await OrderModel.findById(id);

		if (order) res.status(200).json({ order });
		else res.status(404).json({ message: 'Order not found' });
	}
);

export const payOrder = asyncExpressHandler(
	async (req: Request, res: Response) => {
		const order = await OrderModel.findById(req.params.id);
		if (order) {
			order.isPaid = true;
			order.paidAt = Date.now();
			order.payment.paymentResult = {
				payerID: req.body.payerID,
				paymentID: req.body.paymentID,
				orderID: req.body.paymentID,
			};
			const updateOrder = await order.save();
			res.status(200).json({ message: 'Order Paid', order: updateOrder });
		} else {
			res.status(404).json({ message: 'Order not Found' });
		}
	}
);

export const getOrdersByUserId = asyncExpressHandler(
	async (req: Request, res: Response) => {
		const orders = await OrderModel.find({ user: req.body.user._id });
		res.send(orders);
	}
);
