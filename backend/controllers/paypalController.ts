import { Request, Response } from 'express';
import { PAYPAL_CLIENT_ID } from '../server/config';

export const getPaypalClientId = (req: Request, res: Response): void => {
	res.status(200).json({ clientId: PAYPAL_CLIENT_ID });
};
