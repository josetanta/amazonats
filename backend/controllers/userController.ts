import { Request, Response } from 'express';
import asyncExpressHandler from 'express-async-handler';
import UserModel from '../models/userModel';
import { generateToken } from '../utils/utils';

// Create User Admin
export const getCreateAdmin = asyncExpressHandler(
	async (req: Request, res: Response): Promise<void> => {
		try {
			const createUser = {
				name: 'admin',
				email: 'admin@mail.com',
				password: 'password',
				isAdmin: true,
			};

			const user = await UserModel.create(createUser);

			res.status(201).json(user);
		} catch (err: any) {
			res.status(500).json({ message: err?.message });
		}
	}
);

export const signin = asyncExpressHandler(
	async (req: Request, res: Response): Promise<void> => {
		const signUser = await UserModel.findOne({
			email: req.body.email,
			password: req.body.password,
		});

		if (!signUser) {
			res.status(401).json({
				message: 'Invalid Credentials',
			});
		} else {
			res.status(200).json({
				_id: signUser._id,
				name: signUser.name,
				email: signUser.email,
				isAdmin: signUser.isAdmin,
				token: generateToken(signUser),
			});
		}
	}
);

export const register = asyncExpressHandler(
	async (req: Request, res: Response): Promise<void> => {
		const user = new UserModel(req.body);
		const createdUser = await user.save();
		if (!createdUser) {
			res.status(401).json({
				message: 'Invalid User Data',
			});
		} else {
			res.status(201).json({
				_id: createdUser._id,
				name: createdUser.name,
				email: createdUser.email,
				isAdmin: createdUser.isAdmin,
				token: generateToken(createdUser),
			});
		}
	}
);

export const update = asyncExpressHandler(
	async (req: Request, res: Response): Promise<void> => {
		const id = req.params.id;
		const user = await UserModel.findById(id);

		if (!user) {
			res.status(404).json({
				message: 'User not found',
			});
		} else {
			user.name = req.body.name || user.name;
			user.email = req.body.email || user.email;
			user.password = req.body.password || user.password;

			const userSave = await user.save();

			res.status(200).json({
				_id: userSave._id,
				name: userSave.name,
				email: userSave.email,
				isAdmin: userSave.isAdmin,
				token: generateToken(userSave),
			});
		}
	}
);

export const getUsers = asyncExpressHandler(
	async (req: Request, res: Response): Promise<void> => {
		const userList = await UserModel.find();
		res.status(200).json({ users: userList });
	}
);
