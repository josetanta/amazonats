import jwt from 'jsonwebtoken';
import * as config from '../server/config';
import { NextFunction, Request, Response } from 'express';
import { User } from '../models/interfaces';

export function generateToken(user: User): string {
  return jwt.sign(
    {
      _id: user._id,
      name: user.name,
      email: user.email,
      isAdmin: user.isAdmin,
    },
    config.JWT_SECRET
  );
}

export function isAuth(req: Request, res: Response, next: NextFunction): void {
  const bearerToken = req.headers.authorization;

  if (!bearerToken) res.status(401).json({ message: 'Token is not supplied' });
  else {
    const token = bearerToken.slice(7, bearerToken.length);
    jwt.verify(token, config.JWT_SECRET, (err: unknown, data: unknown) => {
      if (err) res.status(401).json({ message: 'Invalid token' });
      else {
        req.body.user = data;
        next();
      }
    });
  }
}
