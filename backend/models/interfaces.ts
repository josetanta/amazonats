import { Document, Schema } from "mongoose";
import { Payment, Shipping } from "./typeModels";

interface Product extends Document {
	name: string;
	description: string;
	category: string;
	brand: string;
	image: string;
	price: number;
	countInStock: number;
	rating: number;
	numReviews: number;
}

interface User extends Document {
	name: string;
	email: string;
	password: string;
	isAdmin: boolean;
}

interface Order extends Document<Order> {
	orderItems: Item[];
	user: User;
	shipping: Shipping;
	payment: Payment;

	itemsPrice: number;
	taxPrice: number;
	shippingPrice: number;
	totalPrice: number;

	isPaid: boolean;
	paidAt: number | string;
	isDelivered: boolean;
	deliveredAt: number | string;
}

interface Item extends Document<Item> {
	product: Schema.Types.ObjectId;
	name: string;
	image: string;
	price: number;
	qty: number;
	countInStock: number;

	category?: string;
	brand?: string;
	rating?: number;
	numReviews?: number;
}

export { Item, Order, Product, User };
