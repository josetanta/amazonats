type Shipping = {
	address: string;
	city: string;
	postalCode: string;
	country: string;
};

type Payment = {
	paymentMethod: string;
	paymentResult: {
		orderID: string;
		payerID: string;
		paymentID: string;
	};
};

export { Shipping, Payment };
