import { Schema, model } from 'mongoose';
import { User } from './interfaces';

const UserSchema = new Schema<User>({
	name: { type: String, required: true },
	email: { type: String, required: true, index: true, unique: true },
	password: { type: String, required: true },
	isAdmin: { type: Boolean, required: true, default: false },
});

const UserModel = model<User>('User', UserSchema);

export default UserModel;
