import { model, Schema } from 'mongoose';
import { Product } from './interfaces';

const ProductSchema = new Schema<Product>(
	{
		name: { type: String, required: true },
		description: { type: String, required: true },
		category: { type: String, required: true },
		brand: { type: String, required: true },
		image: { type: String, required: true },
		price: { type: Number, default: 0.0, required: true },
		countInStock: { type: Number, default: 0, required: true },
		rating: { type: Number, default: 0.0, required: true },
		numReviews: { type: Number, default: 0, required: true },
	},
	{
		timestamps: true,
	}
);

const ProductModel = model<Product>('Product', ProductSchema);

export default ProductModel;
